#include <HX711.h>
#include <PID_v1.h>

#define pinDT  20
#define pinSCK  21
#define pinBotao 53

HX711 scale;

float medida = 0;
const int waterLevelPin = A0;
const int pumpPin = 9;

int estadoBotao = LOW;
int ultimoEstadoBotao = LOW;
unsigned long tempoDebounce = 50;
unsigned long ultimoTempoDebounce = 0;

double setpoint = 50.0;
double input, output;
double Kp = 1.0, Ki = 0.0, Kd = 0.0;
PID pid(&input, &output, &setpoint, Kp, Ki, Kd, DIRECT);

//void controlPump();

void setup() {
  Serial.begin(57600);

  scale.begin(pinDT, pinSCK);
  scale.set_scale(-18337);

  delay(2000);
  scale.tare();

  Serial.println("Balança Zerada");

  pinMode(pumpPin, OUTPUT);
  pinMode(pinBotao, INPUT);

  pid.SetMode(AUTOMATIC);
}

void loop() {
  medida = scale.get_units(5);
  Serial.print("peso: ");
  Serial.println(medida, 3);
  

  scale.power_down();
  delay(1000);
  scale.power_up();

  input = analogRead(waterLevelPin);
  pid.Compute();  // Calcular saída do PID
  controlPump();

  int leituraBotao = digitalRead(pinBotao);

  if (leituraBotao != ultimoEstadoBotao) {
    ultimoTempoDebounce = millis();
  }

  if ((millis() - ultimoTempoDebounce) > tempoDebounce) {
    if (leituraBotao != estadoBotao) {
      estadoBotao = leituraBotao;

      if (estadoBotao == HIGH) {
        if (pid.GetMode() == AUTOMATIC) {
          pid.SetMode(MANUAL);
          // Inicializar a saída manual com um valor específico, por exemplo, 150 (ajuste conforme necessário)
          output = 150;
        } else {
          pid.SetMode(AUTOMATIC);
        }
      }
    }
  }

  ultimoEstadoBotao = leituraBotao;

  delay(100);
}

void controlPump() {
  if (pid.GetMode() == MANUAL) {
    // Se estiver no modo manual, usar diretamente a saída definida
    int pumpSpeed = int(output);
    analogWrite(pumpPin, pumpSpeed);
  } else {
    // Se estiver no modo automático, usar a lógica de controle original
    if (output > 255.0) {
      output = 255.0;
    } else if (output < 0.0) {
      output = 0.0;
    }

    int pumpSpeed = int(output);
    analogWrite(pumpPin, pumpSpeed);
  }
}
