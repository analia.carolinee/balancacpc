// INCLUSÃO DE BIBLIOTECAS
//arduido feito por geovana e maria eduarda aut3d1
#include <HX711.h>
#include <PID_v1.h>

// DEFINIÇÕES DE PINOS
#define pinDT  20
#define pinSCK  21
#define pinBotao 53

// INSTANCIANDO OBJETOS
HX711 scale;

// DECLARAÇÃO DE VARIÁVEIS
float medida = 0;
// Pinos do sensor de nível de água
const int waterLevelPin = A0;

// Pino de controle da bomba (assumindo um relé)
const int pumpPin = 9;
// Defina os parâmetros do PID
double setpoint = 50.0; // Nível desejado da água
double input, output;
double Kp = 1.0, Ki = 0.0, Kd = 0.0; // Ajuste esses valores conforme necessário
PID pid(&input, &output, &setpoint, Kp, Ki, Kd, DIRECT);
void setup() {
  Serial.begin(57600);

  scale.begin(pinDT, pinSCK); // CONFIGURANDO OS PINOS DA BALANÇA
  scale.set_scale(-18337); // LIMPANDO O VALOR DA ESCALA

  delay(2000);
  scale.tare(); // ZERANDO A BALANÇA PARA DESCONSIDERAR A MASSA DA ESTRUTURA

  Serial.println("Balança Zerada");
    // Configurar pino de controle da bomba
  pinMode(pumpPin, OUTPUT);
    // Iniciar PID em modo automático
  pid.SetMode(AUTOMATIC);
}

void loop() {

  medida = scale.get_units(5); // SALVANDO NA VARIAVEL O VALOR DA MÉDIA DE 5 MEDIDAS
  Serial.println(medida, 3); // ENVIANDO PARA MONITOR SERIAL A MEDIDA COM 3 CASAS DECIMAIS

  scale.power_down(); // DESLIGANDO O SENSOR
  delay(1000); // AGUARDA 5 SEGUNDOS
  scale.power_up(); // LIGANDO O SENSOR
    // Medir o nível de água
  input = analogRead(waterLevelPin);
   // Ajustar o estado da bomba com base na saída do PID
  controlPump(output);

  // Aguardar um curto período de tempo
  delay(100);
}

void controlPump(double output) {
  // Limite a saída para evitar ativar ou desativar a bomba bruscamente
  if (output > 255.0) {
    output = 255.0;
  } else if (output < 0.0) {
    output = 0.0;
  }

  // Mapear a saída PID para o intervalo de controle da bomba
  int pumpSpeed = int(output);

  // Ativar ou desativar a bomba com base na saída do PID
  analogWrite(pumpPin, pumpSpeed);
}
